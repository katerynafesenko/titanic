import math

import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for 
    column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    titles = ["Mr.", "Mrs.", "Miss."]
    result_list = []
    for t in titles:
        temp_df = df[df["Name"].str.contains(t.replace(".", "\."))]
        missing_age_number = temp_df["Age"].isnull().sum()
        mean = math.floor(temp_df["Age"].median())
        result_list.append((t, missing_age_number, mean))
    return result_list
